LHCbWebDIRAC is the LHCb extension of [WebAppDIRAC](https://github.com/DIRACGrid/WebAppDIRAC).

# Important links

- Official source code repo: https://gitlab.cern.ch/lhcb-dirac/LHCbWebDIRAC
- Developers Mailing list: https://groups.cern.ch/group/lhcb-dirac/default.aspx

# Install

For more detailed installation instructions, see the [WebAppDIRAC documentation](https://dirac.readthedocs.io/en/latest/DeveloperGuide/WebAppDIRAC/index.html).
